# *linuxcc-prepare*

## Description

Fast script to prepare `kernel-headers` for a module cross-compilation.
Only debian kernel headers supported, but others can work with this approach.

# How to run

- First, install and put your ARM toolchain in the PATH
- Download the exact kernel headers for your target (i.e. `linux-headers-4.19.96+.deb`)
- Gather your `config.gz` from your target (you could tipically find it under
  `/proc/config.gz`). If you can't find it, mount the configfs and it should
	be there.
- Run the script in the same directory where you put the headers DEB package and
  the `config.gz` file (there should be no other DEB package in the same directory):

    $ linuxcc-prepare 4.19.96+ config.gz

# Compile

Although the `linuxcc-prepare` script is ready to run, if you need, you can
modify the source `linuxcc-prepare.sh` and run `make` to build a new
self-containing script for your needs. You need the `shar` utility to compile.

Please, contact me if you need something: robarago@gmail.com
