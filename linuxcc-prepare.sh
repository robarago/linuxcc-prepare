#!/bin/sh
if [ "$1" == "" -o "$2" == "" ]
then
	me=`basename $0`
	echo "$me: $me <kernel version> <config.gz> - Use \`uname -r\` in your target, please"
else
	ccversion=`arm-linux-gnueabihf-gcc -dumpversion 2>/dev/null | tr "." " " | xargs printf "%d%03d%03d" `
	if [ $ccversion -lt 4009004 ]
	then
		echo "arm-linux-gnueabihf-gcc >= 4.9.4 not in PATH - Please, install the toolchain to continue"
	else
		config=`readlink -f $2`
		sharstart=$((`grep -n ^exit$ $0 | cut -f1 -d:`+1))
		echo Toolchain version $ccversion found.
		printf "Descompressing linux-headers... " && dpkg -x *.deb . && rm -r lib usr/share && echo OK || echo ERROR
		if [ ! -d "usr/src/linux-headers-$1" ]
		then
			echo "ERROR: Linux kernel folder linux-headers-$1 not found in DEB contents, only:"
			ls -1 usr/src
			exit
		fi
		kdir=`readlink -f usr/src/linux-headers-$1`
		printf "Restoring kernel config... " && cd $kdir && zcat $config > .config && echo OK || echo ERROR
		printf "Removing native scripts and objects... " && find . -name *.o -delete && find . -type f -executable | xargs file | grep 32-bit | cut -f1 -d: | xargs rm && echo OK || echo ERROR
		printf "Configuring kernel for ARM... \n"
		make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- olddefconfig
		printf "Copying extra sources needed for CC... \n" && echo "" > scripts/selinux/Makefile 2>/dev/null && tail -n +$sharstart $0 | sh
		printf "Making required script for CC... \n"
		make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- scripts/mod/
		echo "DONE!"
		echo "You can now run \`make ARCH=arm CROSS_COMPILE=arm-linux-gnueabihf- -C $kdir M=\<your module dir\> modules\`"
	fi
fi
exit
